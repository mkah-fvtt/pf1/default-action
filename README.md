# Easy Default Action for Pathfinder 1e

Allows partial access to default action data within item sheet.

## Limitations

Conditionals and Miscellaneous are omitted intentionally.

Description is not editable as there seems to be a technical limitation with Foundry that prevents it.

Only available for PF1 0.82.2 as I can't trust this won't break items or actions on other versions.

## Install

Manifest URL: <https://gitlab.com/mkah-fvtt/pf1/default-action/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
