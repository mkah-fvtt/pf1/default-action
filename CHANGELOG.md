# Change Log

## 0.1.0.3

- Fix: Closing item sheet could cause default action's description to become corrupt (e.g. inline rolls would turn into HTML).

## 0.1.0.2

- Change: Skip display on sheets with no actions that aren't editable.
- Fix: Various errors when viewing item not on actor.

## 0.1.0.1

- Properly mark as PF1 module and limit it to 0.82.2

## 0.1.0 Initial
