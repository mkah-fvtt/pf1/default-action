const MODULE_ID = 'pf1-easy-default-action';

/* global libWrapper, pf1 */

let tabTemplate;
Hooks.once('setup', () => getTemplate(`modules/${MODULE_ID}/template/action-tab.hbs`).then(t => tabTemplate = t));

/**
 * Pseudo-activateListeners()
 * @param {ItemSheet} sheet
 */
const activateListeners = (sheet, html) => {
	const item = sheet.item;
	const action = item.firstAction;

	// Open Action Sheet button
	html.querySelector('.open-action-sheet')?.addEventListener('click', ev => {
		ev.preventDefault();
		new pf1.applications.component.ItemActionSheet(action).render(true, { focus: true });
	});

	// Create Default Action
	html.querySelector('.create-default-action')?.addEventListener('click', ev => {
		ev.preventDefault();
		const newActionData = {
			img: item.img,
			name: ['weapon', 'attack'].includes(item.type)
				? game.i18n.localize('PF1.Attack')
				: game.i18n.localize('PF1.Use'),
		};

		return pf1.components.ItemAction.create([newActionData], { parent: item });
	});

	// EXTRA ATTACKS
	html.querySelectorAll('.attack-parts .attack-control').forEach(el => {
		el.addEventListener('click', ev => {
			ev.preventDefault();
			const attackParts = action.data.attackParts;
			if (el.classList.contains('add-attack')) {
				action.update({ attackParts: attackParts.concat([['', '']]) });
			}
			else if (el.classList.contains('delete-attack')) {
				//
				const idx = parseInt(el.closest('.attack-part').dataset.attackPart);
				attackParts.splice(idx, 1);
				action.update({ attackParts });
			}
		});
	});

	// DAMAGE
	html.querySelectorAll('.damage[data-key] .damage-control').forEach(el => {
		el.addEventListener('click', ev => {
			ev.preventDefault();
			const list = el.closest('.damage');
			const k = list.dataset.key;
			const [key, subkey] = k.split('.');
			const damageData = getProperty(action.data, key);
			const damageSubData = getProperty(damageData, subkey);

			if (el.classList.contains('add-damage')) {
				const damageTypeBase = pf1.components.ItemAction.defaultDamageType;
				action.update({ [k]: damageSubData.concat([['', damageTypeBase]]) });
			}
			else if (el.classList.contains('delete-damage')) {
				const part = el.closest('.damage-part[data-damage-part]');
				const idx = parseInt(part.dataset.damagePart);
				damageSubData.splice(idx, 1);
				action.update({ [k]: damageSubData });
			}
		});
	});

	// DAMAGE TYPE
	html.querySelectorAll('.damage .damage-type-visual').forEach(el => {
		el.addEventListener('click', ev => {
			ev.preventDefault();
			const damageIndex = el.closest('.damage-part')?.dataset.damagePart;
			const damagePart = el.closest('.damage')?.dataset.key;
			if (damageIndex != null && damagePart != null) {
				new pf1.applications.DamageTypeSelector(
					action,
					`${damagePart}.${damageIndex}.1`,
					getProperty(action.data, damagePart)[0][1]
				).render(true);
			}
		});
	});

	// NOTES
	html.querySelectorAll('.notes .entry-control a').forEach(el => {
		const key = el.closest('.notes').dataset.name;
		if (!key) return console.error('Invalid notes key', el);

		el.addEventListener('click', ev => {
			ev.preventDefault();

			if (el.classList.contains('add-entry')) {
				const notes = action.data[key];
				action.update({ [key]: notes.concat('') });
			}
			else if (el.classList.contains('delete-entry')) {
				const notes = action.data[key];
				const index = parseInt(el.closest('.entry[data-index]').dataset.index);
				notes.splice(index, 1);
				action.update({ [key]: notes });
			}
		});
	});
}

/**
 * @param {ItemSheet} sheet
 * @param {JQuery} html
 * @param {Object} options
 */
const renderDefaultAction = async (sheet, [html], options) => {
	const item = sheet.item;
	if (item.system.actions === undefined) return;

	const action = item.firstAction;
	if (!action && !sheet.isEditable) return;

	const tabs = html.querySelector('form > nav.tabs');
	const detailsTab = tabs?.querySelector(':scope > [data-tab="details"]');
	if (!detailsTab) return;
	const navItem = document.createElement('a');
	navItem.dataset.tab = 'default-action';
	navItem.dataset.group = 'primary';
	navItem.classList.add('item');
	navItem.textContent = 'Default Action';
	detailsTab.after(navItem);

	const actionData = action?.data;

	const rollData = action?.getRollData() ?? {};

	const templateData = {
		item,
		action,
		actor: item.actor,
		rollData,
		// Following is as expected by PF1's templates
		itemName: item.name,
		enrichedDescription: actionData ? await TextEditor.enrichHTML(actionData.description, { rollData, async: true }) : undefined,
		itemEnh: item.system.enh ?? 0,
		isSpell: item.type === 'spell',
		usesSpellPoints: item.spellbook?.spellPoints.useSystem ?? false,
		canUseAmmo: actionData?.usesAmmo !== undefined,
		owned: item.actor != null,
		parentOwned: item.parentActor != null,
		owner: item.isOwner,
		isGM: game.user.isGM,
		unchainedActionEconomy: game.settings.get('pf1', 'unchainedActionEconomy'),
		hasActivationType: (game.settings.get('pf1', 'unchainedActionEconomy') && actionData?.unchainedAction.activation.type) || (!game.settings.get('pf1', 'unchainedActionEconomy') && actionData?.activation.type),
		showMaxRangeIncrements: actionData?.range?.units === 'ft',
		isWeaponAttack: item.type === 'attack' && item.system.attackType === 'weapon',
		isNaturalAttack: item.type === 'attack' && item.system.attackType === 'natural',
		distanceUnits: deepClone(CONFIG.PF1.distanceUnits),
		damageTypes: pf1.registry.damageTypes.toRecord(),
		tag: pf1.utils.createTag(action?.name ?? ''),
		hasAttackRoll: action?.hasAttack,
		isHealing: actionData?.actionType === 'heal',
		isCombatManeuver: ['mcman', 'rcman'].includes(actionData?.actionType),
		isCharged: action?.isCharged,
		isSelfCharged: action?.isSelfCharged,
		showMaxChargeFormula: ['day', 'week', 'charges'].includes(actionData?.uses?.self?.per),
		canInputRange: action?.hasRange ? ['ft', 'mi', 'spec'].includes(actionData?.range?.units) : undefined,
		canInputMinRange: action?.hasRange ? ['ft', 'mi', 'spec'].includes(actionData?.range?.minUnits) : undefined,
		canInputDuration: actionData?.duration != null && !['', 'inst', 'perm', 'seeText'].includes(actionData?.duration?.units),
		data: actionData,
		config: CONFIG.PF1
	};

	// PF1 doesn't like spell ranges on non-spells
	if (item.type !== 'spell') {
		for (const d of ['close', 'medium', 'long'])
			delete templateData.distanceUnits[d];
	}

	// RENDER
	let tabElem = document.createElement('div');
	tabElem.innerHTML = tabTemplate(templateData, { allowProtoMethodsByDefault: true, allowProtoPropertiesByDefault: true });
	tabElem = tabElem.firstElementChild;

	// Ensure easily identifiable name attribute
	tabElem.querySelectorAll('[name]').forEach(el => el.name = `_defaultAction.${el.name}`);

	// Add to item sheet
	html.querySelector('form > section')
		.append(tabElem);

	// Select & scroll
	tabs.addEventListener('click', ev => sheet._defaultActionTabActive = ev.target.matches('.item[data-tab="default-action"]'), { passive: true });

	tabElem.addEventListener('scroll', ev => {
		sheet._defaultActionScrollPos = ev.target.scrollTop;
	}, { passive: true });

	if (sheet._defaultActionTabActive) sheet._tabs[0].activate('default-action');
	tabElem.scrollTop = sheet._defaultActionScrollPos ?? 0;

	activateListeners(sheet, tabElem);
}

async function sheetItemUpdate(wrapped, event, formData) {
	const data = expandObject(formData);
	const actionData = data._defaultAction;
	if (actionData) {
		delete data._defaultAction;
		const action = this.document.firstAction;
		const diff = diffObject(action.data, actionData);
		if (!foundry.utils.isEmpty(diff))
			await action.update(actionData);
	}

	return wrapped.call(this, event, foundry.utils.flattenObject(data));
}

Hooks.once('init', () => {
	const id = Hooks.on('renderItemSheet', renderDefaultAction);

	// Shift the handler to be first, to ensure sheet enrichers work correctly
	/** @type {Object[]} */
	const events = Hooks.events.renderItemSheet;
	const idx = events.findIndex(h => h.id === id);
	const [handler] = events.splice(idx, 1);
	events.unshift(handler);

	libWrapper.register(MODULE_ID, 'pf1.applications.item.ItemSheetPF.prototype._updateObject', sheetItemUpdate, libWrapper.MIXED);
});
